;
; SciTE4AutoHotkey MsgBox Creator
;

#SingleInstance Ignore
#NoTrayIcon
#NoEnv
SetWorkingDir, %A_ScriptDir%

oSciTE := GetSciTEInstance()
if !oSciTE
{
	MsgBox, 16, MsgBox Creator, Cannot find SciTE!
	ExitApp
}


scHwnd := oSciTE.SciTEHandle

; Main icon
Menu, Tray, Icon, ..\toolicon.icl, 12
vers := 1.1
FileDelete, %A_MyDocuments%\GTA San Andreas User Files\check_version.ini
URL := "https://bitbucket.org/mfsoftru/check_versions/raw/17a996ec00462dfb409a0cf52d2ac8ac1fa5a494/check_version.ini"
UrlDownloadToFile, %URL%, %A_MyDocuments%\GTA San Andreas User Files\check_version.ini
IniRead, version, %A_MyDocuments%\GTA San Andreas User Files\check_version.ini,SciteUDF,version, 1.1
FileRead, out, %A_MyDocuments%\GTA San Andreas User Files\check_version.ini

if (vers != version) {
	MsgBox, 0, Scite4AHK, � ��� �������� ������ Addon UDF ��� Scite4AHK. ���������� ��������� �� ������ ( https://bitbucket.org/mfsoftru/samp-udf/ ) � �������� ����� ������ ������ 
} else {
HTTP := ComObjCreate("WinHTTP.WinHTTPRequest.5.1")
HTTP.SetTimeouts(2000,2000,2000,2000)
HTTP.Open("GET", "http://sa-mp.com/")
HTTP.Send()
checkversion :=  HTTP.ResponseText
RegExMatch(checkversion, "<b>SA-MP ([0-9.]{3,9})</b></font></td>", Res)
StringReplace, Res1, Res1, ., , All

IfNotExist, %A_MyDocuments%/GTA San Andreas User Files/UDF_%Res1%.ahk
{
	MsgBox, 0, Scite4AHK, Scite4AHK �� ����� � ��� ���� UDF_%Res1%.ahk `n������ ���������� ���������� �����
	URL := "https://bitbucket.org/mfsoftru/samp-udf/raw/767f594fcd2584424b76a944a7d81f60ce63fcb4/SAMP-UDF-Developer.ahk"
	UrlDownloadToFile, %URL%, %A_MyDocuments%\GTA San Andreas User Files\UDF_%Res1%.ahk
}
IfNotExist, %A_MyDocuments%/GTA San Andreas User Files/API.dll
{
	MsgBox, 0, Scite4AHK, Scite4AHK �� ����� � ��� ���� API.dll `n������ ���������� ���������� �����
	URL := "https://bitbucket.org/mfsoftru/samp-udf/raw/767f594fcd2584424b76a944a7d81f60ce63fcb4/dx9_overlay.dll"
	UrlDownloadToFile, %URL%, %A_MyDocuments%\GTA San Andreas User Files\API.dll
}
IfNotExist, %A_MyDocuments%/GTA San Andreas User Files/overlay.ahk
{
	MsgBox, 0, Scite4AHK, Scite4AHK �� ����� � ��� ���� overlay.ahk `n������ ���������� ���������� �����
	URL := "https://bitbucket.org/mfsoftru/samp-udf/raw/767f594fcd2584424b76a944a7d81f60ce63fcb4/overlay.ahk"
	UrlDownloadToFile, %URL%, %A_MyDocuments%\GTA San Andreas User Files\overlay.ahk
}

oSciTE.InsertText("; ������ ���������� " vers "`n; ����� ����������� FordeD  | https://vk.com/mcfreeman`n; ��� ���������� SAMP_UDF ��� " Res1 " ������ SA:MP`n")
oSciTE.InsertText("#Include " A_MyDocuments "\GTA San Andreas User Files\overlay.ahk")
oSciTE.InsertText("#Include " A_MyDocuments "\GTA San Andreas User Files\UDF_" Res1 ".ahk")
}